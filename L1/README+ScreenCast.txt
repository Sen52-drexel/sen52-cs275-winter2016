README:

Design - 
URL is created by constructing a string with my Wunderground API key
Request connects to server and pulls back all JSON data available, then disconnects from the server
My program uses GSON to parse the JSON block, and get lat/longitude/city/state

Create new URL using city/state from the information retrieved above
Request reconnects to the new URL, and asks for some more JSON data
This time, I just loop through the array under the root object and print the forecast

Implementation -
Used Gson and HttpUrlConnection objects in Java

Screencast link:
http://screencast-o-matic.com/watch/cDVb3FhEsj

 