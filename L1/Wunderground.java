package test;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class Wunderground {

	public static void main(String[] args) throws Exception {
		String key, city, state;
		Double lat, longitude;
		
	
		if(args.length < 1) {
			System.out.println("Enter Key: ");
			
			Scanner in = new Scanner(System.in);
			key = in.nextLine();
		}
		else {
			key = args[0];
		}
		//key = "0177e6cde1ddc433";
		
		String sURL = "http://api.wunderground.com/api/" + key + "/geolookup/q/autoip.json";
		
		URL url = new URL(sURL);
		HttpURLConnection request = (HttpURLConnection) url.openConnection();
		request.connect();
		
		JsonParser jp = new JsonParser();
		JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
		JsonObject rootobj = root.getAsJsonObject();
		
		//ZIP,City,State
		
		lat = rootobj.get("location").getAsJsonObject().get("lat").getAsDouble();
		longitude = rootobj.get("location").getAsJsonObject().get("lon").getAsDouble();
		city = rootobj.get("location").getAsJsonObject().get("city").getAsString();
		state = rootobj.get("location").getAsJsonObject().get("state").getAsString();
		
		request.disconnect();
		
		System.out.println("Latitude: " + lat);
		System.out.println("Longitude: "+ longitude + "\n");
		
		sURL = "http://api.wunderground.com/api/" + key + "/hourly/q/" + state + "/" + city + ".json";
		
		URL url2 = new URL(sURL);
		request = (HttpURLConnection) url2.openConnection();
		request.connect();
		
		root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
		rootobj = root.getAsJsonObject();
		
		JsonArray arr = rootobj.getAsJsonArray ("hourly_forecast"); 
		for (JsonElement obj: arr){ 
			//Prints current date, conditions, temperature, humidity, and
			System.out.println (obj.getAsJsonObject().get ("FCTTIME").getAsJsonObject().get("pretty").getAsString() + ":"); 
			System.out.println ("- Conditions:" + obj.getAsJsonObject().get("condition").getAsString ());
			System.out.println ("- Temperature: " + obj.getAsJsonObject().get("temp").getAsJsonObject().get("english").getAsString () + " fahrenheit");
			System.out.println ("- Humidity: " + obj.getAsJsonObject().get("humidity").getAsString ());
		}
	}
}
