import java.util.Scanner;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.temboo.Library.Google.Calendar.GetAllCalendars;
import com.temboo.Library.Google.Calendar.GetAllCalendars.GetAllCalendarsInputSet;
import com.temboo.Library.Google.Calendar.GetAllCalendars.GetAllCalendarsResultSet;
import com.temboo.Library.Google.Calendar.GetAllEvents;
import com.temboo.Library.Google.Calendar.GetAllEvents.GetAllEventsInputSet;
import com.temboo.Library.Google.Calendar.GetAllEvents.GetAllEventsResultSet;
import com.temboo.Library.Google.OAuth.FinalizeOAuth;
import com.temboo.Library.Google.OAuth.FinalizeOAuth.FinalizeOAuthInputSet;
import com.temboo.Library.Google.OAuth.FinalizeOAuth.FinalizeOAuthResultSet;
import com.temboo.Library.Google.OAuth.InitializeOAuth;
import com.temboo.Library.Google.OAuth.InitializeOAuth.InitializeOAuthInputSet;
import com.temboo.Library.Google.OAuth.InitializeOAuth.InitializeOAuthResultSet;
import com.temboo.core.TembooException;
import com.temboo.core.TembooSession;

public class OauthLab {
	
public static void main(String[] args) throws TembooException{
	Scanner scanner = new Scanner(System.in);
	//Temboo get all calendars
	// Instantiate the Choreo, using a previously instantiated TembooSession object, eg:
	String client_id = "895129336006-2r14j1tknppr3s0pcqfh20drr5tr2lka.apps.googleusercontent.com";
	String secret = "WMmmr6vX2lgX39-N6e2CC1G9";
	String redirect_uri = "http://developers.google.com";
	TembooSession session = new TembooSession("sen52", "myFirstApp", "cb93809093c849fbbd4f351e9043cff5");
	
	InitializeOAuth initializeOAuthChoreo = new InitializeOAuth(session);
	// Get an InputSet object for the choreo
	InitializeOAuthInputSet initializeOAuthInputs = initializeOAuthChoreo.newInputSet();
	initializeOAuthInputs.set_ForwardingURL("http://developers.google.com");
	initializeOAuthInputs.set_Scope("https://www.googleapis.com/auth/calendar https://www.googleapis.com/auth/calendar.readonly");
	initializeOAuthInputs.set_ClientID(client_id);
	// Execute Choreo
	InitializeOAuthResultSet initializeOAuthResults = initializeOAuthChoreo.execute(initializeOAuthInputs);
	
	String callback_id = initializeOAuthResults.get_CallbackID();
	System.out.println("Go to this link and click Allow\n" + initializeOAuthResults.get_AuthorizationURL());
	
	
	
	
	FinalizeOAuth finalizeOAuthChoreo = new FinalizeOAuth(session);
	// Get an InputSet object for the choreo
	FinalizeOAuthInputSet finalizeOAuthInputs = finalizeOAuthChoreo.newInputSet();
	finalizeOAuthInputs.set_ClientSecret(secret);
	finalizeOAuthInputs.set_CallbackID(callback_id);
	finalizeOAuthInputs.set_ClientID(client_id);
	// Execute Choreo
	FinalizeOAuthResultSet finalizeOAuthResults = finalizeOAuthChoreo.execute(finalizeOAuthInputs);
	
	
	/* GET ALL CALENDARS */
	GetAllCalendars getAllCalendarsChoreo = new GetAllCalendars(session);
	// Get an InputSet object for the choreo
	GetAllCalendarsInputSet getAllCalendarsInputs = getAllCalendarsChoreo.newInputSet();
	getAllCalendarsInputs.set_ClientSecret("WMmmr6vX2lgX39-N6e2CC1G9");
	getAllCalendarsInputs.set_ClientID("895129336006-2r14j1tknppr3s0pcqfh20drr5tr2lka.apps.googleusercontent.com");
	getAllCalendarsInputs.set_AccessToken(finalizeOAuthResults.get_AccessToken());
	// Execute Choreo
	GetAllCalendarsResultSet getAllCalendarsResults = getAllCalendarsChoreo.execute(getAllCalendarsInputs);
	
	
    JsonParser jp = new JsonParser();
    JsonElement root = jp.parse(getAllCalendarsResults.get_Response());
    JsonObject rootobj = root.getAsJsonObject();

    JsonArray items = rootobj.get("items").getAsJsonArray();
    JsonObject item = items.get(1).getAsJsonObject();
    String cal_id = item.get ("id").getAsString();

	

    GetAllEvents getAllEventsChoreo = new GetAllEvents(session);

    // Get an InputSet object for the choreo
    GetAllEventsInputSet getAllEventsInputs = getAllEventsChoreo.newInputSet();
    getAllEventsInputs.set_AccessToken(finalizeOAuthResults.get_AccessToken());
    getAllEventsInputs.set_CalendarID(cal_id);
    getAllEventsInputs.set_ClientSecret(secret);
    getAllEventsInputs.set_ClientID(client_id);
    // Execute Choreo
    GetAllEventsResultSet getAllEventsResults = getAllEventsChoreo.execute(getAllEventsInputs);
    root = jp.parse(getAllEventsResults.get_Response());
    rootobj = root.getAsJsonObject();
    
    JsonArray events = rootobj.get("items").getAsJsonArray();
    for(int i = 0; i < events.size(); i++){
    	JsonObject event = events.get(i).getAsJsonObject();
    	System.out.print(event.get("start").getAsJsonObject().get("date").getAsString() + " ");
    	System.out.println(event.get("summary").getAsString());
    }
    
}
}

