package dropbox;
//app name: sen52_CS275_Assignment1
//app key : yuh1hvai4sowlah
//app secret: 5fodnnutk4fcjol

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Locale;
import java.util.Scanner;

import com.dropbox.core.DbxAppInfo;
import com.dropbox.core.DbxAuthFinish;
import com.dropbox.core.DbxAuthInfo;
import com.dropbox.core.DbxException;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.DbxWebAuthNoRedirect;
import com.dropbox.core.json.JsonReader;

public class Dbox {
	
	public static void main(String[] args)
	        throws IOException
	    {
			//A lot of the OAuth code is from a repository for Dropbox OAuth at 
			//https://github.com/dropbox/dropbox-sdk-java/blob/master/examples/authorize/src/com/dropbox/core/examples/authorize/Main.java
			//By Karl Rieb
	        String argAppInfoFile = "/Users/Sam/Dropbox/Move/app_info.txt";
	        String argAuthFileOutput = "/Users/Sam/Dropbox/Move/auth_output.txt";

	        // Read app info file (contains app key and app secret)
	        DbxAppInfo appInfo;
	        try {
	            appInfo = DbxAppInfo.Reader.readFromFile(argAppInfoFile);
	        }
	        catch (JsonReader.FileLoadException ex) {
	            System.err.println("Error reading <app-info-file>: " + ex.getMessage());
	            System.exit(1); return;
	        }

	        // Run through Dropbox API authorization process
	        String userLocale = Locale.getDefault().toString();
	        DbxRequestConfig requestConfig = new DbxRequestConfig("examples-authorize", userLocale);
	        DbxWebAuthNoRedirect webAuth = new DbxWebAuthNoRedirect(requestConfig, appInfo);

	        String authorizeUrl = webAuth.start();
	        System.out.println("1. Go to " + authorizeUrl);
	        System.out.println("2. Click \"Allow\" (you might have to log in first).");
	        System.out.println("3. Copy the authorization code.");
	        System.out.print("Enter the authorization code here: ");

	        String code = new BufferedReader(new InputStreamReader(System.in)).readLine();
	        if (code == null) {
	            System.exit(1); return;
	        }
	        code = code.trim();

	        DbxAuthFinish authFinish;
	        try {
	            authFinish = webAuth.finish(code);
	        }
	        catch (DbxException ex) {
	            System.err.println("Error in DbxWebAuth.start: " + ex.getMessage());
	            System.exit(1); return;
	        }

	        System.out.println("Authorization complete.");
	        System.out.println("- User ID: " + authFinish.userId);
	        System.out.println("- Access Token: " + authFinish.accessToken);

	        // Save auth information to output file.
	        DbxAuthInfo authInfo = new DbxAuthInfo(authFinish.accessToken, appInfo.host);
	        try {
	            DbxAuthInfo.Writer.writeToFile(authInfo, argAuthFileOutput);
	            System.out.println("Saved authorization information to \"" + argAuthFileOutput + "\".");
	        }
	        catch (IOException ex) {
	            System.err.println("Error saving to <auth-file-out>: " + ex.getMessage());
	            System.err.println("Dumping to stderr instead:");
	            DbxAuthInfo.Writer.writeToStream(authInfo, System.err);
	            System.exit(1); return;
	        }
	        
	        //Done with authorization process - Time to do some file shtuff
	        
	        String dbxPath = "/Users/Sam/Dropbox/Move/"; //Dropbox path for the "move" folder 
	        String list = "/Users/Sam/Dropbox/Move/__list.txt/"; //__list file location
	        File listFile= new java.io.File(list);
	        Path listpath = Paths.get(list);
	        Scanner fscan = new Scanner(listFile);

	        File tempFile = new File("/Users/Sam/Dropbox/Move/temp.txt");
	        BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile));
	        //Loop through the __list file, and move 
	        while (fscan.hasNextLine()){

	        	String line = fscan.nextLine(); //Get line and parse it
	        	String [] seperate = line.split("\\s+", 2); 
	        	if (seperate.length != 2){ //if line is not formatted properly, 
	        		System.out.println("A line could not be read"); //notify the user and continue
	        		writer.write(line);
	        		writer.newLine();
	        		continue; 
	        	}
	        	
	        	String fname = seperate[0];
	        	String dest = seperate[1]; 
	        	if(dest.charAt(dest.length()-1) != '/')
	        		dest += "/";
	        	
	        	//Move files 
	        	java.io.File source = new java.io.File(dbxPath + fname);
	        	java.io.File destination = new java.io.File (dest + fname);
	        	Files.copy (source.toPath (), destination.toPath ());
	        	System.out.println("Moved " + fname + " to " + dest);
	       }
	        writer.close();
	        fscan.close();
	        Files.delete(listpath);
	        tempFile.renameTo(new java.io.File("/Users/Sam/Dropbox/Move/__list.txt"));
	       						
	    }

}
