README:

Design/Implementation:

Goal of this app is to 'search your Dropbox entries for files in a directory called "move."  A file within this directory called "__list" will specify how to move files.' I did not use temboo for this program - I authenticated directly through Dropbox. Karl Rieb gave a fantastic example for authentication at https://github.com/dropbox/dropbox-sdk-java/blob/master/examples/authorize/src/com/dropbox/core/examples/authorize/Main.java, so I used that for the first part of the program. After all of the OAuth authentication was done, I simply used Dropbox's (fantastic) Mac interface, which allowed me to move files around using the specifications in the list file. What I'm wondering is if I actually needed to use OAuth in this program (I could have just moved files around on my computer because of how well Dropbox is integrated with OS X).

User guide: 

Under your dropbox folder, create a folder called "Move", and put into it a text document called "__list.txt". Edit __list.txt so that it contains the files you want to move, and where you want to move them to, formatted as so:
A.txt /home/wmm24
B.txt /home/abc123

Run the program - When prompted, open the authentication link in your browser, and press "allow". Copy the authentication code from Dropbox and enter it back into the program. All done. After each run, the items that were sucessfully moved in __list.txt will be removed 


