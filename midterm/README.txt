README:

Implementation/Design:

Simple Temboo OAuth for twitter - once I have the finalize results set, I can run another choreo to get the user's timeline. Once I have that, I can get it as an array and put it into a string. From here, I can pass it to the smog_grade() function (which calculates a smog grade from it). 
In order to figure out how many sentences there are: count the number of occurences of !?.: (we discount the occurences that were part of invalid words)
In order to figure out the number of polysyllable words, I sent requests to wordnik's API. 
Once I had both of these variables, the grade was calculated using this formula:
grade = 1.0430 * sqrt(number_of_polysyllables * (30 / number_of_sentences)) + 3.1291 

One problem with using wordnik is that it's pretty slow to keep making http requests -- using a regex would have been much faster, but less accurate

User instructions:

Run the program - when prompted, go to the authorization link and click "Authorize App" to allow access to your tweets. Once you've authorized the program, go back to the console and press enter. All polysyllable words that you tweeted will appear on screen, as well as the number of polysyllable words and your smog grade.

SOURCES:

Temboo
Wordnik API