package cs275_midterm;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.temboo.Library.Twitter.OAuth.FinalizeOAuth;
import com.temboo.Library.Twitter.OAuth.FinalizeOAuth.FinalizeOAuthInputSet;
import com.temboo.Library.Twitter.OAuth.FinalizeOAuth.FinalizeOAuthResultSet;
import com.temboo.Library.Twitter.OAuth.InitializeOAuth;
import com.temboo.Library.Twitter.OAuth.InitializeOAuth.InitializeOAuthInputSet;
import com.temboo.Library.Twitter.OAuth.InitializeOAuth.InitializeOAuthResultSet;
import com.temboo.Library.Twitter.Timelines.UserTimeline;
import com.temboo.Library.Twitter.Timelines.UserTimeline.UserTimelineInputSet;
import com.temboo.Library.Twitter.Timelines.UserTimeline.UserTimelineResultSet;
import com.temboo.core.TembooException;
import com.temboo.core.TembooSession;

public class tweetreader {

	public static void main(String []args) throws TembooException, IOException {
		
	Scanner stdin = new Scanner(System.in);	
	//All Temboo
	TembooSession session = new TembooSession("sen52", "myFirstApp", "cb93809093c849fbbd4f351e9043cff5");

	InitializeOAuth initializeOAuthChoreo = new InitializeOAuth(session);

	// Get an InputSet object for the choreo
	InitializeOAuthInputSet initializeOAuthInputs = initializeOAuthChoreo.newInputSet();
	/* Yes, I 'hard coded' these values - copypasta from Temboo */
	initializeOAuthInputs.set_ConsumerKey("PGVgf7B9f7cnUToenMWqr653I");
	initializeOAuthInputs.set_ConsumerSecret("sUcDHZxFvWe5ajrA7wPUQ2CwOG2C1gHKVdnISxt3p0DAqjHI8T"); 
	// Execute Choreo
	InitializeOAuthResultSet initializeOAuthResults = initializeOAuthChoreo.execute(initializeOAuthInputs);
	
	System.out.println("Go to the following URL and click \"Authorize App\"");
	System.out.println(initializeOAuthResults.get_AuthorizationURL());
	System.out.println("Press enter when done");
	String wait = stdin.nextLine();

	FinalizeOAuth finalizeOAuthChoreo = new FinalizeOAuth(session);

	// Get an InputSet object for the choreo
	FinalizeOAuthInputSet finalizeOAuthInputs = finalizeOAuthChoreo.newInputSet();
	finalizeOAuthInputs.set_ConsumerKey("PGVgf7B9f7cnUToenMWqr653I");
	finalizeOAuthInputs.set_ConsumerSecret("sUcDHZxFvWe5ajrA7wPUQ2CwOG2C1gHKVdnISxt3p0DAqjHI8T");
	finalizeOAuthInputs.set_CallbackID(initializeOAuthResults.get_CallbackID());
	finalizeOAuthInputs.set_OAuthTokenSecret(initializeOAuthResults.get_OAuthTokenSecret());

	// Execute Choreo
	FinalizeOAuthResultSet finalizeOAuthResults = finalizeOAuthChoreo.execute(finalizeOAuthInputs);
	
	System.out.println("UR ACCOUNT DUN BEEN OAUTH'D");
	
	UserTimeline userTimelineChoreo = new UserTimeline(session);
	UserTimelineInputSet userTimelineInputs = userTimelineChoreo.newInputSet();

	userTimelineInputs.set_ConsumerKey("PGVgf7B9f7cnUToenMWqr653I");
	userTimelineInputs.set_ConsumerSecret("sUcDHZxFvWe5ajrA7wPUQ2CwOG2C1gHKVdnISxt3p0DAqjHI8T");
	userTimelineInputs.set_AccessToken(finalizeOAuthResults.get_AccessToken());
	userTimelineInputs.set_AccessTokenSecret(finalizeOAuthResults.get_AccessTokenSecret());
	userTimelineInputs.set_UserId(finalizeOAuthResults.get_UserID());
	userTimelineInputs.set_Count(30);
	
	UserTimelineResultSet userTimelineResults = userTimelineChoreo.execute(userTimelineInputs);
	String JSONResponse = userTimelineResults.get_Response();
	String jsonString = "{array:" + JSONResponse + "}";
	
	JsonParser jp = new JsonParser();
	JsonElement root = jp.parse(jsonString);
	JsonObject rootobj = root.getAsJsonObject();
	
	JsonArray jarray = rootobj.get("array").getAsJsonArray();
	String tweets = ""; 
	for (int i = 0; i < jarray.size(); i++) {
		tweets += jarray.get(i).getAsJsonObject().get("text").getAsString();
	}
	
	double grade = smog_grade(tweets);
	
	//  I said "This twitter", because judging someones reading score based off of their 
	// social media account is highly judgemental (and my score was trash)
	System.out.println("This twitter's SMOG grade level is " + grade);
	
	} //End of main function
	
	/*
	 * double smog_grade(string)
	 * Takes a String 
	 * returns a double of a user's smog score, based off of this formula:
	 * grade = 1.0430 * sqrt(number_of_polysyllables * (30 / number_of_sentences)) + 3.1291
	 * Apparently nothing is below a 3rd grade level
	 * 
	 * I use wordnik's api to calculate the number of syllables of each word
	 * Because I have to make a bunch of requests, this function is pretty slow.
	 */
	public static double smog_grade(String tweets) throws IOException{
		String wordnik_key = "6b535a5b52611d4d6b83d0f2b7101b282f54ed99321432506";
		
		int number_of_polysyllables = 0;
		int number_of_sentences = tweets.split("[!?.:]+ ").length; 
		String[] words = tweets.split(" ");
		
		for(int i = 0; i < words.length; i++){ //Goes through the array of words and makes sure none of them are non-standard 'words'
			if(!words[i].matches("[a-zA-Z]+")){ 
				//if(words[i].matches("(.*)[!?.:](.*)"))
					//number_of_sentences--;     //If the word had a !?.: in it, the number of sentences is reduced by 1 
				continue;						//(sentences were split by these parameters at the top of the function)
			}
			String sURL = "http://api.wordnik.com/v4/word.json/"+ words[i] +"/hyphenation?useCanonical=false&limit=50&api_key="+wordnik_key;
			
			URL url = new URL(sURL);
			HttpURLConnection request = (HttpURLConnection) url.openConnection();
			request.connect();
		
			JsonParser jp = new JsonParser();
			JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
			String jsonString = "{array:" + root.toString() + "}"; //because I didn't like their response
			root = jp.parse(jsonString);
			JsonObject rootobj = root.getAsJsonObject();
			
			int num_sylls = rootobj.get("array").getAsJsonArray().size();
			
			if(num_sylls > 1){
				number_of_polysyllables++;
				System.out.println(words[i] + " is a polysyllable.");
			}
		}
		System.out.println("Number of Polysyllables: " + number_of_polysyllables +"\n");
		return  (1.0430 * Math.sqrt(number_of_polysyllables * (30.0 / number_of_sentences)) + 3.1291); //formula
	}
}
